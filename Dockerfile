FROM java:8 as builder

WORKDIR /app

ADD build.gradle .
ADD gradlew .
ADD gradle/ gradle

# For caching of faster builds
RUN ./gradlew

ADD src/ src
RUN ./gradlew build



FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
COPY --from=builder /app/build/libs/*.jar mn-web-app-j.jar
EXPOSE 5000
CMD java  -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar mn-web-app-j.jar
